Cory Ferraez is an attorney from Hattiesburg, Mississippi. He was named Top 40 Under 40 in 2018 by the Association of American Trial Lawyers.

Website: https://callcoryms.com/
